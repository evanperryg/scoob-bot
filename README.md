# scoob-bot
A Discord music bot that interfaces with Last.FM and YouTube to provide advanced features. When queueing music, the user can 
simply type the name of a song, and scoob-bot will use the last.fm API to find the closest match to the given title, find 
data about that song (including the song title compensated for minor spelling errors, as well as artist, album, last.fm playcount,
album art, etc). Using this data, scoob-bot then finds an appropriate YouTube link for that song, and will play from that link.

The user may also simply queue a youtube link, or any other link that is compatible with lavaplayer. If a link to a YouTube video is given to the bot by the $queue command, it will attempt to parse the video's title to determine song metadata.

Any song which scoob-bot can find metadata for (that is, any song queued with $queue (artist):(title) or $queue (title), and most songs queued with $queue (youtube url)) can be scrobbled to a last.fm account.

The last.fm account which receives the scrobbles can be provided in the keys.json file. If the credentials for the account aren't provided in keys.json, the user will be prompted to enter their account credentials into the console.

Scoob-bot also gives access to other unique features, including the ability to see a list of recently-played tracks, and the 
ability to go back to a previous track.

[Check out evanperryg's scoob-bot's play history here!](https://www.last.fm/user/scoob-bot)

### Feature Screenshots
- [Queueing a track by song title](https://i.imgur.com/2u9CviA.jpg)
- [Queueing a track by artist and song title](https://i.imgur.com/Qxcfc0v.jpg)
- [Queueing a track by YouTube link](https://i.imgur.com/aYaPvre.jpg) 
- [The "upnext" command](https://i.imgur.com/bNrCrWF.jpg)
- [The "recent" command](https://i.imgur.com/xyxOtNX.jpg)
- [Scoob-bot's Discord Presence shows the current track](https://i.imgur.com/DSdK6Ly.jpg)
- [The "artistinfo" command](https://i.imgur.com/UeyFXYf.jpg)
- When a new track starts playing, [scoob-bot shows the song title, artist, album, and cover art](https://i.imgur.com/FsgNARw.jpg)

### [Check the Wiki for information for developers and server owners.](https://github.com/evanperryg/scoob-bot/wiki)
