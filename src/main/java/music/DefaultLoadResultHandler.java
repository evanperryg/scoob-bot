package music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * This AudioLoadResultHandler places a song at the back of the queue. This is the usual way of
 * loading in a song.
 */
public class DefaultLoadResultHandler implements AudioLoadResultHandler {
    private Song song;
    private TextChannel textChannel = MusicPlayer.musicPlayer.getTextChannel();

    public DefaultLoadResultHandler(Song song) {
        this.song = song;
    }

    @Override
    public void trackLoaded(AudioTrack track) {
        if(song.getTitle().equals("(unknown)"))
            textChannel.sendMessage("Queueing song, but metadata could not be determined.").queue();
        else {
            textChannel.sendMessage("Queueing: **" +
                    song.getTitle() + "** by **" + song.getArtist() + "**").queue();
        }

        MusicPlayer.trackScheduler.queue(track, song);
    }

    @Override
    public void playlistLoaded(AudioPlaylist playlist) {
        textChannel.sendMessage("Scoob-bot is not currently compatible with URL playlists.").queue();
    }

    @Override
    public void noMatches() {
        textChannel.sendMessage("No matches!").queue();
    }

    @Override
    public void loadFailed(FriendlyException exception) {
        textChannel.sendMessage("Load failed :(").queue();
    }
}