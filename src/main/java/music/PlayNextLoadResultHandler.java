package music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * PlayNextLoadResultHandler differs from the DefaultLoadResultHandler in only one way- instead of putting a track at
 * the back of the queue, PlayNextLoadResultHandler puts it at the front of the queue.
 */
public class PlayNextLoadResultHandler implements AudioLoadResultHandler {
    private Song song;
    private TextChannel textChannel = MusicPlayer.musicPlayer.getTextChannel();

    public PlayNextLoadResultHandler(Song song) {
        this.song = song;
    }

    @Override
    public void trackLoaded(AudioTrack track) {
        if(song.getTitle().equals("(unknown)"))
            textChannel.sendMessage("Adding song to top of queue, but metadata could not be determined.").queue();
        else {
            textChannel.sendMessage("Queueing next: **" +
                    song.getTitle() + "** by **" + song.getArtist() + "**").queue();
        }

        MusicPlayer.trackScheduler.queueNext(track, song);
    }

    @Override
    public void playlistLoaded(AudioPlaylist playlist) {
        textChannel.sendMessage("Scoob-bot is not currently compatible with URL playlists.").queue();
    }

    @Override
    public void noMatches() {
        textChannel.sendMessage("No matches!").queue();
    }

    @Override
    public void loadFailed(FriendlyException exception) {
        textChannel.sendMessage("Load failed :(").queue();
    }
}