package music;

import api.LastFM;
import api.Youtube;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import com.sedmelluq.discord.lavaplayer.track.BaseAudioTrack;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import static main.Globals.logger;

/**
 * Created by evan on 5/1/2018.
 * The Song object holds data and links relating to a song. This stuff is retrieved from last.fm. The Song object may
 * also hold a lavaplayer-compatible link to where the song can be played (this is stored in the {@code playurl} field).
 */
public class Song {
    /** contains the song title, given by the constructor */
    protected String title = "(unknown)";

    /** contains the artist name. may be given by the constructor (recommended),
     * or Song can be constructed without specifying the artist's name.
     */
    protected String artist = "(unknown)";

    /** contains the name of the album, as provided by last.fm's track.getinfo */
    protected String album = "(unknown)";

    /** contains the link to the album art for the song, as provided by last.fm's track.getinfo */
    protected String albumicon = "(unknown)";

    /** link to last.fm artist page, as provided by last.fm's track.getInfo. */
    protected String artistlink = "(unknown)";

    /** link to last.fm song page, as provided by last.fm's track.getInfo. */
    protected String songlink = "(unknown)";

    /** duration of the song in milliseconds, as provided by last.fm's track.getInfo. */
    protected String duration = "(unknown)";

    /**
     * objects which extend upon Song may use playurl to contain a url which links to
     * a place where the song can be played.
     */
    protected String playurl = "(unknown)";

    /**
     * objects which extend upon Song may use status to contain additional information.
     * example of this can be seen in YoutubeSong.
     */
    protected String status = "Retrieval method did not specify a status.";

    /**
     * if a Song object has valid entries for artist and title, this may be set to true.
     */
    protected boolean canBeScrobbled = false;

    public Song() {

    }

    /**
     * Performs a single last.fm track.getInfo API request and fills in the above fields with the results.
     * @param title the song title
     * @param artist the artist's name
     */
    public Song(String title, String artist) {
        // perform a last.fm track.getInfo request and populate fields from results.
        try {
            JSONObject trackdata = LastFM.getSongInfo(artist, title).getJSONObject("track");

            this.title      = trackdata.getString("name");
            this.songlink   = trackdata.getString("url");
            this.duration   = trackdata.getString("duration");

            JSONObject artistdata = trackdata.getJSONObject("artist");
            this.artist     = artistdata.getString("name");
            this.artistlink = artistdata.getString("url");

            JSONObject albumdata = trackdata.getJSONObject("album");
            this.album = albumdata.getString("title");
            this.albumicon = albumdata.getJSONArray("image")
                    .getJSONObject(2).getString("#text");

            canBeScrobbled = true;
        } catch (IOException e) {
            logger.error("IOException thrown while attempting to populate fields for Song!");
        }
    }

    /**
     * This version of the constructor allows the creation of a song object without
     * specifying the artist's name. It requires two requests to the last.fm API instead of
     * one, because it searches last.fm for the most appropriate match to the given title.
     * @param title the song title to search for
     */
    public Song(String title) {
        // perform a last.fm track.search request
        try {
            JSONArray searchResults = LastFM.songSearch(title);

            // this algorithm finds the search result which is most similar to the given song title,
            int resultStrength = 0;
            String reftitle = cleanseString(title).trim();
            this.title = title; // just in case nothing shows up

            for(int i = 0; i < searchResults.length(); i++) {
                JSONObject result = searchResults.getJSONObject(i);
                int compval = compareStrings(reftitle, cleanseString(result.getString("name")).trim());
                if(compval > resultStrength) {
                    resultStrength = compval;
                    this.title  = result.getString("name");
                    this.artist = result.getString("artist");
                }
            }

            // this part is the same as the other constructor.
            // perform a last.fm track.getInfo request and populate fields from results.
            JSONObject trackdata = LastFM.getSongInfo(artist, title).getJSONObject("track");
            this.title      = trackdata.getString("name");
            this.songlink   = trackdata.getString("url");
            this.duration   = trackdata.getString("duration");

            JSONObject artistdata = trackdata.getJSONObject("artist");
            this.artist     = artistdata.getString("name");
            this.artistlink = artistdata.getString("url");

            JSONObject albumdata = trackdata.getJSONObject("album");
            this.album = albumdata.getString("title");
            this.albumicon = albumdata.getJSONArray("image")
                    .getJSONObject(2).getString("#text");

            canBeScrobbled = true;
        } catch (IOException e) {
            logger.error("IOException thrown while attempting to populate fields for Song!");
        }
    }

    public static Song songFromUrl(String address) {
        Song out = new Song();
        try {
            logger.info("attempting to retrieve matadata using HTML source for given URL");
            JSONObject json = Youtube.getVideo(address);

            json = json.getJSONObject("snippet");
            String videoName = json.getString("title").replace("\"","");

            // the stuff below makes me nauseous but it does the job
            String[] tryTheseDelimiters = {" - ", "- ", "-", ": ", ":", " by "};
            boolean somethingWorked = false;

            // attempt 2: remove "featuring" artists and crap in parenthesis following title, then
            // do a basic delimiter split
            String[] cutoffs = {" ft", "\\(", "\\["};
            for(String tryACutoff : cutoffs) {
                try {
                    String tryString = videoName.split(tryACutoff)[0];
                    for (String tryThis : tryTheseDelimiters) {
                        if (tryString.contains(tryThis) && (!somethingWorked)) {
                            String split_0 = trimLeadingAndTrailingSpaces(tryString.split(tryThis)[0]);
                            String split_1 = trimLeadingAndTrailingSpaces(tryString.split(tryThis)[1]);
                            try {
                                logger.info("attempting \"" + split_1 + "\" by \"" + split_0 + "\"");
                                out = new YoutubeSong(split_1, split_0);
                                somethingWorked = true;
                            } catch (JSONException j) {
                                try {
                                    logger.info("attempting \"" + split_0 + "\" by \"" + split_1 + "\"");
                                    out = new YoutubeSong(split_0, split_1);
                                    somethingWorked = true;
                                } catch (JSONException k) {}
                            }
                        }
                    }
                } catch (IndexOutOfBoundsException e) {}
            }

            // attempt x: just brute force the video name through and see what happens
            if(!somethingWorked) {
                try {
                    out = new YoutubeSong(videoName);
                    somethingWorked = true;
                } catch (JSONException j) {}
            }

            // this has to be below the metadata finder stuff, because YoutubeSong is gonna try to put in its own
            // status and playurl, so we need to overwrite them
            out.playurl = address;
            out.status  = "Direct URL Retrieval";

            out.canBeScrobbled = somethingWorked;
            if(!somethingWorked)
                out.albumicon = json.getJSONObject("thumbnails").getJSONObject("default").getString("url");
            return out;
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("YouTube video data parsing failed to return metadata.");
        out.albumicon = "Album art unavailable due to retrieval method.";
        out.canBeScrobbled = false;
        out.playurl = address;
        out.status  = "Direct URL Retrieval";
        return out;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getAlbumicon() {
        return albumicon;
    }

    public String getArtistlink() {
        return artistlink;
    }

    public String getSonglink() {
        return songlink;
    }

    public String getDuration() {
        return duration;
    }

    /**
     * Gives public access to the stored value of playurl.
     * @return the URL where the song can be played.
     */
    public String getPlayurl() {
        return playurl;
    }

    /**
     * Gives public access to the stored value of status.
     * @return supplementary information about the song and/or playurl.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Gives public access to the stored value of canBeScrobbled.
     * @return true if the song can be scrobbled, false otherwise.
     */
    public boolean canBeScrobbled() {
        return canBeScrobbled;
    }

    @Override
    public String toString() {
        return String.format("%-20s%-18s%-38s",title,artist,status);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Song)) return false;
        Song song = (Song) obj;
        return song.getTitle().equals(title) && song.getArtist().equals(artist);
    }

    public static String toStringHeader() {
        return String.format("%-20s%-18s%-38s","Title","Artist","Status");
    }

    /**
     * Returns a number representing how "similar" two strings are. If many comparisons
     * are made against a single string, the single string should be given as the ref
     * parameter, and the other strings should be given as the compare parameter.
     * @param ref the reference string. In the application here, this will be the
     *            user-entered song title.
     * @param compare the compare string. In the application here, this will be the
     *                song titles returned by last.fm's track.search operation.
     * @return out number of times ref and compare have the same char at a given index.
     */
    private static int compareStrings(String ref, String compare) {
        int out = 0;
        int len = ref.length();
        if(compare.length() < ref.length()) len = compare.length();
        for(int i = 0; i < len; i++) {
            if(ref.charAt(i) == compare.charAt(i)) ++out;
        }
        return out;
    }

    private static int floor(int i) {
        if(i < 0) return 0;
        else return i;
    }

    private static String trimLeadingAndTrailingSpaces(String str) {
        if(str.charAt(0) == ' ')
            return trimLeadingAndTrailingSpaces(str.substring(1));
        else if(str.charAt(str.length() - 1) == ' ')
            return trimLeadingAndTrailingSpaces(str.substring(0, str.length() - 1));
        else return str;
    }

    protected static String cleanseString(String str) {
        return str.replaceAll("[^a-zA-Z]+", " ").trim().toLowerCase();
    }

}
