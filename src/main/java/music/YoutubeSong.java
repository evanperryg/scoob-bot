package music;

import api.Youtube;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * YoutubeSong inherits Song, populating Song's playurl field with a Youtube link.
 * <p>
 *     When a YoutubeSong is instantiated, it will perform the same requests for metadata that the Song constructor
 *     performs. Once the metadata has been populated, the YoutubeSong constructor will then perform a YouTube Data API
 *     request which will search for a YouTube video which matches the song's metadata. There is an algorithm that
 *     determines the best YouTube link to play the song from, which is explained in the getYoutubeLink() method
 *     description.
 * </p>
 * playurl format: http://www.youtube.com/watch?v=VIDEOKEY
 */
public class YoutubeSong extends Song {
    public YoutubeSong(String title, String artist) {
        super(title, artist);
        getYoutubeLink();
    }

    public YoutubeSong(String title) {
        super(title);
        getYoutubeLink();
    }

    private YoutubeSong() {

    }

    /**
     * This function assigns values to {@code playurl} and {@code status} using the YouTube Data API's search utility.
     * <p>
     *     The first step is to filter out live recordings. By searching for "(live" in the title, we remove live
     *     recordings without interfering with song titles or artist names that include the sequence of characters
     *     "live." We also filter out results that have the phrase "live at" in the description. Thanks to RedElixir
     *     for offering this idea.
     * </p>
     * <p>
     *     YouTube automatically generates channels which have titles in the form "(Artist Name) - Topic." These
     *     channels generally have the original (or most popular) masters of a particular artist's songs, and they are
     *     encoded in a high-quality format. Since this is the most consistent "good" source for music, the algorithm
     *     will always return a result from one of these channels, if it finds one. Some artists also have their own
     *     YouTube channels, whose titles are simply the artist's name. Obviously, a video directly from the artist is
     *     also a good result, so we will immediately return a result from one of these sources as well. Otherwise, we
     *     will also look for VEVO results that don't contain the term "music video" in the description. If it doesn't
     *     find any of these, it will simply return the highest result in the YouTube search.
     * </p>
     *
     */
    private void getYoutubeLink() {
        JSONArray results = Youtube.getSearchResults(artist + " " + title, 12);
        boolean foundPreferredResult = false;      // set this to true if a preferred YT video is found in the for loop
        for(int i = 0; i < results.length() ; i++) {
            JSONObject item = results.getJSONObject(i);

            JSONObject snippet  = item.getJSONObject("snippet");
            String channelTitle = snippet.getString("channelTitle");
            String description  = snippet.getString("description");
            String videoTitle   = snippet.getString("title");

            if(!(videoTitle.toLowerCase().contains("(live") || description.contains("live at")))
                if(snippet.getString("channelTitle").contains("VEVO") &&
                        (!description.toLowerCase().contains("music video")) &&
                        (cleanseString(videoTitle).contains(cleanseString(title)))) {
                    playurl = "http://www.youtube.com/watch?v=" + item.getJSONObject("id").getString("videoId");
                    status  = "VEVO YouTube link";
                    return;     // this is a preferred result, so set the flag true
                } else if( ((channelTitle.contains(artist) && channelTitle.contains("Topic")) ||
                           (channelTitle.equals(artist) && videoTitle.contains(title)))
                       && ((title.toLowerCase().contains("live")) || (!videoTitle.toLowerCase().contains("live")))
                       && ((artist.toLowerCase().contains("live")) || (!videoTitle.toLowerCase().contains("live")))
                       && ((title.toLowerCase().contains("concert")) || (!videoTitle.toLowerCase().contains("concert")))
                       && ((artist.toLowerCase().contains("concert")) || (!videoTitle.toLowerCase().contains("concert")))
                       && videoTitle.toLowerCase().contains(title.toLowerCase())) {
                    playurl = "http://www.youtube.com/watch?v=" + item.getJSONObject("id").getString("videoId");
                    status  = "Artist YouTube link";
                    foundPreferredResult = true;   // this is the most preferred result, so just return
                } else if((!foundPreferredResult) && (!videoTitle.toLowerCase().contains("cover"))
                        && (!videoTitle.toLowerCase().contains("live in"))
                        && (!videoTitle.toLowerCase().contains("live at"))
                        && (!videoTitle.toLowerCase().contains("(clean"))
                        && (!videoTitle.toLowerCase().contains("reaction"))
                        && (!videoTitle.toLowerCase().contains("soundcheck"))
                        && ((title.toLowerCase().contains("concert")) || (!videoTitle.toLowerCase().contains("concert")))
                        && ((artist.toLowerCase().contains("concert")) || (!videoTitle.toLowerCase().contains("concert")))
                        && (!videoTitle.toLowerCase().contains("sound check"))) {
                    playurl = "http://www.youtube.com/watch?v=" + item.getJSONObject("id").getString("videoId");
                    status  = "General YouTube link";
                    foundPreferredResult = true;
                }

        }
    }

    /**
     * This can be used to reduce the load on the Last.FM API. Song object will only be populated with an
     * artist, song title, and youtube link.
     */
    public static Song generateYoutubeSongWithoutMetadata(String title, String artist, String status) {
        YoutubeSong out = new YoutubeSong();
        out.artist = artist;
        out.title = title;
        out.getYoutubeLink();
        out.status = status;
        out.canBeScrobbled = true;
        return out;
    }

}
