package music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

/**
 * Created by evan on 5/1/2018.
 */
public class PlayNowLoadResultHandler implements AudioLoadResultHandler {
    private Song songLoaded;

    public PlayNowLoadResultHandler(Song songLoaded) {
        this.songLoaded = songLoaded;
    }

    @Override
    public void trackLoaded(AudioTrack track) {
        MusicPlayer.trackScheduler.forceTrack(track);
    }

    @Override
    public void playlistLoaded(AudioPlaylist playlist) {
        MusicPlayer.trackScheduler.forceTrack(playlist.getTracks().remove(0));
    }

    @Override
    public void noMatches() {
        MusicPlayer.musicPlayer.getTextChannel().sendMessage("No matches!").queue();
    }

    @Override
    public void loadFailed(FriendlyException exception) {
        MusicPlayer.musicPlayer.getTextChannel().sendMessage("Load failed :(").queue();
    }
}