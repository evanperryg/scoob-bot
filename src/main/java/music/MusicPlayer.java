package music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;

import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.managers.AudioManager;

import util.AudioPlayerSendHandler;

import java.lang.ref.WeakReference;

import static main.Globals.PLAYER_VOLUME;
import static main.Globals.logger;

/**
 * Once the music player has been initialized using !startplayer, it will only respond to music-related
 * commands in the text channel in which !startplayer was run.
 */
public class MusicPlayer {
    public static MusicPlayer musicPlayer = null;

    private static AudioPlayerManager playerManager;
    public static AudioPlayer player;
    private static AudioManager audioManager;
    public static TrackScheduler trackScheduler;

    private static volatile boolean playerRunning = false;

    private static TextChannel textChannel;

    /**
     * Use this when starting the musicplayer.
     * @return
     */
    public static MusicPlayer MusicPlayerBuilder(MessageReceivedEvent event)
    throws IllegalStateException {
        playerManager = new DefaultAudioPlayerManager();
        player = playerManager.createPlayer();
        if(playerRunning)
            throw new IllegalStateException("MusicPlayerBuilder called when music player is already running!");

        textChannel = event.getTextChannel();

        AudioSourceManagers.registerRemoteSources(playerManager);

        trackScheduler = new TrackScheduler(player);

        VoiceChannel voiceChannel = null;
        for(VoiceChannel v : event.getGuild().getVoiceChannels()) {
            if(v.getMembers().contains(event.getMember()))
                voiceChannel = v;
        }

        if(voiceChannel == null)
            throw new IllegalStateException("User must be in a voice channel to start music player!");

        audioManager = event.getGuild().getAudioManager();

        player.setVolume(PLAYER_VOLUME);
        player.addListener(trackScheduler);

        audioManager.setSendingHandler(new AudioPlayerSendHandler(player));
        audioManager.openAudioConnection(voiceChannel);

        playerRunning = true;

        return new MusicPlayer();
    }

    /**
     * Use to check to see if the music player is currently active.
     * @return playerRunning
     */
    public static boolean isRunning() {
        return playerRunning;
    }

    /**
     * Call this to terminate the music player.
     */
    public void terminate() {
        player.setPaused(true);
        trackScheduler.stop();
        playerManager.shutdown();

        playerRunning = false;

        audioManager.closeAudioConnection();
    }

    /**
     * Play, aka "unpause" or "resume"
     */
    public void play() {
        if(player.isPaused()) {
            player.setPaused(false);
        } // an else statement could be used here for debugging maybe?
    }

    public void next() {
        trackScheduler.nextTrack();
    }

    public void pause() {
        player.setPaused(true);
        System.gc();    //now's a good time, since we aren't playing music
    }

    public void previous() {
        // basically, what's happening here is that in order to go back one, we have to go back two
        // then go forward one. Don't question it.
        Song originallyPlaying = trackScheduler.nowPlaying;
        queueNextSilent(trackScheduler.nowPlaying);
        queueNextSilent(trackScheduler.recentQueue.dequeueFront());
        trackScheduler.nextTrackNullifyNowPlaying();

        try {
            if (trackScheduler.getNowPlaying().equals(originallyPlaying))
                next();
        } catch (NullPointerException e) {
            // it's a good thing if this gets thrown, so whatever
        }
    }

    // basic structure for queueing a track:
    // 1) the Song to queue is passed to queue(Song song)
    // 2) which then passes the Song's playurl to the playermanager
    // 3) the player manager then passes it to one of the methods in the AudioLoadResultHandler
    // 4) the player manager then puts the song in the track scheduler
    public void queue(Song song) {
        playerManager.loadItem(song.getPlayurl(), new DefaultLoadResultHandler(song));
    }

    public void queueNext(Song song) {
        playerManager.loadItem(song.getPlayurl(), new PlayNextLoadResultHandler(song));
    }

    public void queueNextSilent(Song song) {
        playerManager.loadItem(song.getPlayurl(), new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                MusicPlayer.trackScheduler.queueNext(track, song);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {

            }

            @Override
            public void noMatches() {

            }

            @Override
            public void loadFailed(FriendlyException exception) {

            }
        });
    }

    public void playNow(Song song) {
        playerManager.loadItem(song.getPlayurl(), new PlayNowLoadResultHandler(song));
    }

    public TextChannel getTextChannel() {
        return textChannel;
    }

}
