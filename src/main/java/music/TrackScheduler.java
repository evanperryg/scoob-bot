package music;

import api.LastFM;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import main.ScoobBot;
import net.dv8tion.jda.core.entities.Game;
import util.Deque;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import static main.Globals.*;

/**
 * Created by evan on 5/1/2018. The TrackScheduler does exactly what it does in the LavaPlayer examples,
 * except this one also manages the queues of Songs, allowing for the pretty "now playing" messages, and
 * the ability to go back to previous tracks.
 */
public class TrackScheduler extends AudioEventAdapter {
    private final AudioPlayer player;
    private final BlockingDeque<AudioTrack> queue;
    private long playTime;       // holds the UTC system time that the track started at
    Deque<Song> playQueue;
    Deque<Song> recentQueue;
    Song nowPlaying;

    TrackScheduler(AudioPlayer player) {
        playQueue = new Deque<>();
        recentQueue = new Deque<>();

        this.player = player;
        this.queue = new LinkedBlockingDeque<>();
    }

    void queue(AudioTrack track, Song song) {
        // Calling startTrack with the noInterrupt set to true will start the track only if nothing is currently playing. If
        // something is playing, it returns false and does nothing. In that case the player was already playing so this
        // track goes to the queue instead.
        if(playQueue.getSize() >= PLAYQUEUE_MAXSIZE)
            MusicPlayer.musicPlayer.getTextChannel()
                    .sendMessage("Failed to add song! Queue is at max size (" + PLAYQUEUE_MAXSIZE + ")").queue();
        else {
            playQueue.enqueueRear(song);
            if (!player.startTrack(track, true)) {
                queue.offer(track);
            }
        }
    }

    void queueNext(AudioTrack track, Song song) {
        if(playQueue.getSize() >= PLAYQUEUE_MAXSIZE)
            MusicPlayer.musicPlayer.getTextChannel()
                    .sendMessage("Failed to add song! Queue is at max size (" + PLAYQUEUE_MAXSIZE + ")").queue();
        else {
            playQueue.enqueueFront(song);
            if (!player.startTrack(track, true)) {
                queue.offerFirst(track);
            }
        }
    }

    void nextTrack() {
        // Start the next track, regardless of if something is already playing or not. In case queue was empty, we are
        // giving null to startTrack, which is a valid argument and will simply stop the player.
        player.startTrack(queue.poll(), false);
    }

    void nextTrackNullifyNowPlaying() {
        // Start the next track, regardless of if something is already playing or not. In case queue was empty, we are
        // giving null to startTrack, which is a valid argument and will simply stop the player.
        nowPlaying = null;
        player.startTrack(queue.poll(), false);
    }

    void forceTrack(AudioTrack track) {
        // same thing as nextTrack, but instead of pulling from the queue, play the given AudioTrack.
        player.startTrack(track, false);
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        // check to see if nowPlaying is marked as scrobble-able
        if(nowPlaying.canBeScrobbled()) {
            // see if we should scrobble the track. Track will be scrobbled if at least 50% of it has been played,
            // or 4 minutes of the track has been played, whichever is shorter.
            long songDuration = Long.valueOf(nowPlaying.getDuration());
            long compareVal = ((songDuration > 480000) ? 240000 : (songDuration / 2));
            if ((System.currentTimeMillis() - playTime) >= compareVal)
                LastFM.scrobble(nowPlaying);
        }

        // Only start the next track if the end reason is suitable for it (FINISHED or LOAD_FAILED)
        if (endReason.mayStartNext) {
            System.gc();    //now's a good time, since we're between tracks
            nextTrack();
        }
    }

    @Override
    public void onTrackStart(AudioPlayer player, AudioTrack track) {
        try {
            if(nowPlaying != null) //fixes issue where a null is placed in recentlyplayed when the first song is played
                recentQueue.enqueueFront(nowPlaying);

            if(recentQueue.getSize() > BACKQUEUE_MAXSIZE) recentQueue.dequeueRear();
            nowPlaying = playQueue.dequeueFront();

            ScoobBot.getJda().getPresence().setGame(
                    Game.listening("\"" + nowPlaying.title + "\" by " + nowPlaying.artist));

            playTime = System.currentTimeMillis();

            if(nowPlaying.canBeScrobbled())
                LastFM.setNowListening(nowPlaying);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            MusicPlayer.musicPlayer.getTextChannel()
                    .sendMessage("Track started but no Song found in play queue!").queue();
        }

        // build the automatic now-playing message
        StringBuilder sb = new StringBuilder();
        sb.append("Now Playing: **").append(nowPlaying.getTitle())
                .append("** by **").append(nowPlaying.getArtist()).append("**")
                .append("\n").append(nowPlaying.getStatus());

        if(!nowPlaying.getAlbum().equals("(unknown)")) {
            sb.append("\nAlbum: ").append(nowPlaying.getAlbum());
            if(NOWPLAYING_SHOWART)
                sb.append("\n").append(nowPlaying.getAlbumicon());
        } else {
            // if album title isn't available, then we weren't able to runByMessageReceivedEvent last.fm data.
            // when this is the case, show status to add clarity.
            sb.append("\n").append(nowPlaying.getPlayurl());
        }
        MusicPlayer.musicPlayer.getTextChannel().sendMessage(sb.toString()).queue();
    }

    public Song getNowPlaying() {
        return nowPlaying;
    }

    protected void setNowPlaying(Song song) {
        nowPlaying = song;
    }

//    public Deque<Song> getPlayQueue() {
    public Deque<Song> getPlayQueue() {
        return playQueue;
    }

//    public Deque<Song> getRecentQueue() {
    public Deque<Song> getRecentQueue() {
        return recentQueue;
    }

    public String upNext() {
        return playQueue.toStringReverse();
    }

    public String recentlyPlayed() {
        return recentQueue.toString();
    }

    void stop() {
        queue.clear();
        playQueue = null;
        recentQueue = null;
        nowPlaying = null;
    }

}
