package main;

import api.LastFM;
import command.Command;
import command.CommandManifest;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import util.Kotlog;

import javax.security.auth.login.LoginException;
import java.io.*;

import static main.Globals.*;

/*
 * DONE: make the queue method actually follow the deque size limits
 * DONE: make stopplayer do something other than lock some commands
 * DONE: create a logger class which takes PrintStreams as output parameters
 * DONE: add $back, which plays the previous song
 * TODO: add $nowplaying, which shows complete info for the current track
 * DONE: add $upnext, which shows the contents of the playQueue
 * DONE: add $queuenext, which queues a song to the front of the playQueue instead of the back
 * DONE: add $recent, which shows the contents of the recentQueue
 * DONE: get last.fm account/authentication for bot, add scrobbling functionality
 * TODO: add functionality for saving queues to local file, and reloading them
 * DONE: add support for song data retrieval from YouTube link
 * TODO: make YoutubeSong.getYoutubeLink() set the song album art if the Last.FM API didn't have it
 * DONE: add and implement a json file for the command manifest, where permissions can be set (Reflections may help)
 * TODO: V O I C E C O M M A N D S
 */

/**
 * Created by evan on 4/30/2018. This is the main class for scoob-bot. JDA listener functions are put here.
 * The main() should only be used for initialization procedures.
 */
public class ScoobBot extends ListenerAdapter {
    private static JDA jda;

    public static void main(String[] args) throws LoginException, IOException {
        logger = new Kotlog();
        // if the logger should write to a file, add a printstream for that file here

        JSONObject conf = null, keys = null, perm = null;

        // retrieve conf.json
        try (FileReader confFile = new FileReader(new File(confJSONUrl))){
            conf = new JSONObject(new JSONTokener(confFile));
            confFile.close();       //just to be safe
            logger.info("conf.json was found and loaded.");
        } catch (FileNotFoundException e) {
            logger.critical("conf.json was not found at the specified directory: " + confJSONUrl);
        } catch (JSONException e) {
            logger.critical("conf.json has a formatting error that prevented a JSONObject from being generated.");
            e.printStackTrace();
        }

        // retrieve keys.json
        try (FileReader keysFile = new FileReader(new File(keysJSONUrl))) {
            keys = new JSONObject(new JSONTokener(keysFile));
            keysFile.close();       //just to be safe
            logger.info("keys.json was found and loaded.");
        } catch (FileNotFoundException e) {
            logger.critical("conf.json was not found at dir\n   " + keysJSONUrl);
        } catch (JSONException e) {
            logger.critical("conf.json has a formatting error that prevented a JSONObject from being generated.");
            e.printStackTrace();
        }

        // retrieve perm.json. Although it is not used here, this will ensure that it is available for when
        // CommandManifest uses it.
        try (FileReader permFile = new FileReader(new File(permJSONUrl))) {
            perm = new JSONObject(new JSONTokener(permFile));
            permFile.close();       //just to be safe
            logger.info("perm.json was found and loaded.");
        } catch (FileNotFoundException e) {
            logger.critical("perm.json was not found at dir\n   " + permJSONUrl);
        } catch (JSONException e) {
            logger.critical("perm.json has a formatting error that prevented a JSONObject from being generated.");
            e.printStackTrace();
        }

        CMD_CHAR           = conf.getString("CMD_CHAR").charAt(0);
        PLAYQUEUE_MAXSIZE  = conf.getInt("PLAYQUEUE_MAXSIZE");
        BACKQUEUE_MAXSIZE  = conf.getInt("BACKQUEUE_MAXSIZE");
        PLAYER_VOLUME      = conf.getInt("PLAYER_VOLUME");
        NOWPLAYING_SHOWART = conf.getBoolean("NOWPLAYING_SHOWART");

        DISCORD_TOKEN      = keys.getString("DISCORD_TOKEN");
        YOUTUBE_APIKEY     = keys.getString("YOUTUBE_APIKEY");

        logger.config("discord command character is " + CMD_CHAR);
        logger.config("play queue max size is " + PLAYQUEUE_MAXSIZE);
        logger.config("back queue max size is " + BACKQUEUE_MAXSIZE);
        logger.config("music player volume is " + PLAYER_VOLUME);

        LastFM.init(conf, keys);

        CommandManifest.init();

        // start up JDA
        // difference in clipping between Blocking and Async is negligible
        // NativeAudioSendFactory seems to flat out not work with Async
        jda = new JDABuilder(AccountType.BOT)
                .setToken(DISCORD_TOKEN)
                .addEventListener(new ScoobBot())
                .buildAsync();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        User author = event.getAuthor();
        Message message = event.getMessage();
        String msg = message.getContentDisplay().trim();

        if(author.isBot()) return;

        if(Command.stringIsValidCommand(msg)) {         // check to see if message has command char at beginning
            String cmd = Command.getCommandName(msg);
            logger.info("command received: " + cmd);
            // choose the appropriate command class to execute, and run it.
            CommandManifest.runByMessageReceivedEvent(event).run();

        }
    }

    public static JDA getJda() {
        return jda;
    }
}
