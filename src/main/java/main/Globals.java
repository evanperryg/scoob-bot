package main;

import util.Kotlog;

/**
 * Created by evan on 4/30/2018.
 * Thanks Patrick
 */
public class Globals {
    /** The global instance of util.Kotlog. Should be initialized/configured in the main. */
    public static Kotlog logger;

    /** Path to conf.json */
    public static final String confJSONUrl = "./properties/conf.json";

    /** Path to keys.json */
    public static final String keysJSONUrl = "./properties/private/keys.json";

    /** Path to commandManifest.json */
    public static final String permJSONUrl = "./properties/perm.json";

    /** Discord Bot Token */
    public static String DISCORD_TOKEN = "NULL";

    /** LastFM API Key */
    public static String LASTFM_APIKEY = "NULL";

    /** YouTube Data API Key */
    public static String YOUTUBE_APIKEY = "NULL";

    /** Back queue maximum length */
    public static int BACKQUEUE_MAXSIZE = 20;

    /** Play queue maximum length */
    public static int PLAYQUEUE_MAXSIZE = 200;

    /** Music player volume */
    public static int PLAYER_VOLUME = 50;

    /** Command character */
    public static char CMD_CHAR = '$';

    /** Decides whether the automatic "Now Playing" messages will show cover art. */
    public static boolean NOWPLAYING_SHOWART = true;

    /** Enable/disable last.fm scrobbling. */
    public static boolean ENABLE_SCROBBLING = true;
}
