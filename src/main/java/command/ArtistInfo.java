package command;

import api.LastFM;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static main.Globals.logger;

/**
 * Created by evan on 4/30/2018.
 */
public class ArtistInfo extends Command {
    public static final String name = "artistinfo";
    public static final String description = "Show information about an artist from their Last.FM profile.";

    public ArtistInfo(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        try {
            JSONObject json = LastFM.getArtistInfo(params).getJSONObject("artist");

            event.getChannel().sendMessage("**" + json.getString("name")
                    + "**\t|\tlast.fm listeners: " + json.getJSONObject("stats").getString("listeners")).queue();

            JSONArray similarArtists = json.getJSONObject("similar").getJSONArray("artist");
            event.getChannel().sendMessage("**Similar Artists:** " +
                    similarArtists.getJSONObject(0).getString("name") +
                    ", " + similarArtists.getJSONObject(1).getString("name") +
                    ", " + similarArtists.getJSONObject(2).getString("name") +
                    ", " + similarArtists.getJSONObject(3).getString("name") +
                    ", " + similarArtists.getJSONObject(4).getString("name")).queue();

            event.getChannel().sendMessage(json.getJSONArray("image").getJSONObject(4).getString("#text")).queue();

            String bio = json.getJSONObject("bio").getString("content");
            if(bio.length() > 1200)
                bio = bio.substring(0, bio.indexOf(". ", 1200) + 1);
            event.getChannel().sendMessage(bio).queue();

            event.getChannel().sendMessage("More info: " + json.getString("url")).queue();
        } catch (JSONException e) {
            event.getChannel().sendMessage("Artist not found.").queue();
        } catch (IOException e) {
            logger.error("Command artistinfo failed to connect to last.fm!");
            e.printStackTrace();
        }
    }

}
