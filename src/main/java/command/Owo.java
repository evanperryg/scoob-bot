package command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

/**
 * Created by evan on 5/9/2018.
 */
public class Owo extends Command {
    public static final String name = "owo";
    public static final String description = "what's this?";

    public Owo(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        event.getTextChannel().sendMessage("what's this?").queue();
    }
}
