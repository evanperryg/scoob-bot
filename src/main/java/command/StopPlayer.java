package command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import music.MusicPlayer;

/**
 * This command runs the necessary procedures to turn off music player functionality.
 * Once this command is run, the commands in the musicplayer package (queue, skip, pause, play, etc) will not do
 * anything until the startplayer command is run again.
 */
public class StopPlayer extends Command {
    public static final String name = "stopplayer";
    public static final String description = "Turns off the music player.";

    public StopPlayer(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) return;
        event.getChannel().sendMessage("Stopping Music Player...").queue();
        MusicPlayer.musicPlayer.terminate();
        event.getChannel().sendMessage("Music Player Stopped.").queue();
    }

}
