package command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

/**
 * Created by evan on 5/3/2018.
 * The help command.
 */
public class Help extends Command {
    public static final String name = "help";
    public static final String description = "Display the help menu.";

    public Help(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        event.getTextChannel().sendMessage(CommandManifest.generateHelpMessage()).queue();
    }
}