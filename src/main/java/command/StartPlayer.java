package command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import music.MusicPlayer;

/**
 * This command runs the necessary procedures to turn on music player functionality.
 * When a user runs the $startplayer command, the bot will initialize the music player backend, and connect to whatever
 * voice channel the user is in. Once the "Music Player Startup Complete!" message is seen in the text channel where
 * $startplayer was called, the bot will accept other music player-related commands (queue, skip, pause, play, etc.)
 */
public class StartPlayer extends Command {
    public static final String name = "startplayer";
    public static final String description = "Turn on the music player.";

    public StartPlayer(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(MusicPlayer.isRunning()) return;
        event.getChannel().sendMessage("Starting Music Player...").queue();
        try {
            MusicPlayer.musicPlayer = MusicPlayer.MusicPlayerBuilder(event);
            event.getChannel().sendMessage("Music Player Startup Complete!").queue();
        } catch (IllegalStateException e) {
            event.getChannel().sendMessage("Music player could not start! Are you in a voice channel?").queue();
        }
    }

}
