package command;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import static main.Globals.CMD_CHAR;

/**
 * Created by evan on 4/30/2018. All commands for scoob-bot use Command as their base class.
 *
 * <p>
 *  To add a new command to ScoobBot, first make a class which extends Command, and has a field
 * {@code public static final String name} (the value of the name field is the command that users will use to
 * activate that command). The class will also need a (@code public static final String description}. the description
 * will appear when the help command is used. Put your class anywhere in the command package, and the Command Manifest
 * will automatically detect it at runtime. ALL COMMANDS MUST HAVE A NAME AND DESCRIPTION. BOTH ARE REQUIRED.
 * </p>
 */
public class Command {
    protected String params;
    protected MessageReceivedEvent event;
    public static final String name = "generic";
    public static final String description = null;

    public Command(MessageReceivedEvent event) {
        this.event = event;
        Message message = event.getMessage();
        String cmd = message.getContentDisplay().trim();
        if(cmd.substring(0,cmd.length() - 1).contains(" "))
            params = cmd.substring(cmd.indexOf(' ') + 1);
        else params = "";
    }

    public void run() {
        event.getChannel().sendMessage("Invalid Command!").queue();
    }

    public static boolean stringIsValidCommand(String cmd) {
        return cmd.charAt(0) == CMD_CHAR;
    }

    /**
     * When given a Discord message string, this will return just the name of the command.
     * For example, if given "$foo bar" this method will return "foo"
     * @param cmd A Discord message string which contains a command for the bot.
     * @return The command name given by cmd.
     */
    public static String getCommandName(String cmd) {
        if(cmd.contains(" "))
            return cmd.substring(1,cmd.indexOf(' '));
        else return cmd.substring(1);
    }

    public String getCmdName() {
        return name;
    }

}
