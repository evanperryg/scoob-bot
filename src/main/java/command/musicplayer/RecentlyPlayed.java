package command.musicplayer;

import command.Command;
import music.MusicPlayer;
import music.Song;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

/**
 * Created by evan on 5/2/2018.
 */
public class RecentlyPlayed extends Command {
    public static final String name = "recent";
    public static final String description = "Show recently played tracks.";

    public RecentlyPlayed(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        MusicPlayer.musicPlayer.getTextChannel().sendMessage("```" + Song.toStringHeader() + "\n" +
                MusicPlayer.trackScheduler.recentlyPlayed() + "```").queue();
    }
}
