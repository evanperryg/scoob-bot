package command.musicplayer;

import command.Command;
import music.Song;
import music.YoutubeSong;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import music.MusicPlayer;
import org.json.JSONException;

import static main.Globals.CMD_CHAR;

/**
 * Created by evan on 5/1/2018.
 */
public class Queue extends Command {
    public static final String name = "queue";
    public static final String description = "Put a song in the playback queue.\n"
            + CMD_CHAR + "queue (song title)\n"
            + CMD_CHAR + "queue (artist name): (song title)\n"
            + CMD_CHAR + "queue (youtube video url)";

    public Queue(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        if(params.isEmpty()) {
            event.getTextChannel().sendMessage("You must pass parameters to the queue command!").queue();
            return;
        }

        Song song;
        try {   // build the song object based on the possible entry methods
            if (params.startsWith("www.") || params.startsWith("http")) {
                song = Song.songFromUrl(params);
            } else if (params.contains(":")) {
                params = params.replaceFirst(": ", ":");
                song = new YoutubeSong(params.substring(params.indexOf(':') + 1)
                        , params.substring(0, params.indexOf(':')));
            } else
                song = new YoutubeSong(params);

            MusicPlayer.musicPlayer.queue(song);
        } catch (JSONException e) {
            event.getTextChannel().sendMessage("Song could not be found!").queue();
        }
    }
}
