package command.musicplayer;

import command.Command;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import music.MusicPlayer;

/**
 * This command implements the play/unpause/resume/whatever functionality for the music player
 */
public class Play extends Command {
    public static final String name = "play";
    public static final String description = "Un-pause/resume/whatever";

    public Play(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        if(!params.isEmpty())
            event.getTextChannel().sendMessage("did you mean to use the queue or queuenext command?").queue();
        else MusicPlayer.musicPlayer.play();
    }
}
