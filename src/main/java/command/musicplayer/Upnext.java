package command.musicplayer;

import command.Command;
import music.MusicPlayer;
import music.Song;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.concurrent.BlockingDeque;

/**
 * Created by evan on 5/2/2018.
 */
public class Upnext extends Command {
    public static final String name = "upnext";
    public static final String description = "Shows a list of queued tracks.";

    public Upnext(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        String toShow = MusicPlayer.trackScheduler.upNext();
        if(toShow.length() > 1900) {
            printLongQueue(toShow);
        } else {
            MusicPlayer.musicPlayer.getTextChannel().sendMessage("```" + Song.toStringHeader() + "\n" +
                    toShow + "```").queue();
        }
    }

    private void printLongQueue(String trackList) {
        int upto = trackList.indexOf("\n",1800);
        String toShow = trackList.substring(0,upto);
        MusicPlayer.musicPlayer.getTextChannel().sendMessage("```" + Song.toStringHeader() + "\n" +
                toShow + "```").queue();

        trackList = trackList.substring(upto + 1);
        if(trackList.length() > 1) {
            printLongQueue(trackList);
        }
    }
}
