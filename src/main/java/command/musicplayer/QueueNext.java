package command.musicplayer;

import command.Command;
import music.MusicPlayer;
import music.Song;
import music.YoutubeSong;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.json.JSONException;

import static main.Globals.CMD_CHAR;

/**
 * Created by evan on 5/2/2018.
 */
public class QueueNext extends Command {
    public static final String name = "queuenext";
    public static final String description = "Put a song at the front of the playback queue.\n"
            + CMD_CHAR + "queuenext (song title)\n"
            + CMD_CHAR + "queuenext (artist name): (song title)\n"
            + CMD_CHAR + "queuenext (youtube video url)";

    public QueueNext(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        if(params.isEmpty()) {
            event.getTextChannel().sendMessage("You must pass parameters to the queuenext command!").queue();
            return;
        }

        Song song;
        try {
            if (params.startsWith("www.") || params.startsWith("http")) {
                song = Song.songFromUrl(params);
            } else if (params.contains(":")) {
                params = params.replaceFirst(": ", ":");
                song = new YoutubeSong(params.substring(params.indexOf(':') + 1)
                        , params.substring(0, params.indexOf(':')));
            } else
                song = new YoutubeSong(params);

            MusicPlayer.musicPlayer.queueNext(song);
        } catch (JSONException e) {
            event.getTextChannel().sendMessage("Song could not be found!").queue();
        }
    }
}