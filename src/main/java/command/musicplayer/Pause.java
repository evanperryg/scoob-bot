package command.musicplayer;

import command.Command;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import music.MusicPlayer;

/**
 * This command implements the pause functionality for the music player
 */
public class Pause extends Command {
    public static final String name = "pause";
    public static final String description = "Pause the music player.";

    public Pause(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        MusicPlayer.musicPlayer.pause();
    }
}
