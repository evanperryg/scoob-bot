package command.musicplayer;

import api.LastFM;
import command.Command;
import music.MusicPlayer;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import static main.Globals.ENABLE_SCROBBLING;

/**
 * This command implements the pause functionality for the music player
 */
public class Love extends Command {
    public static final String name = "love";
    public static final String description = "Love the current track on last.fm";

    public Love(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        if(!ENABLE_SCROBBLING) {
            event.getTextChannel().sendMessage("Scrobbling must be enabled use this command!").queue();
            return;
        }
        LastFM.loveTrack(MusicPlayer.trackScheduler.getNowPlaying());
    }
}
