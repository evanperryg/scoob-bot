package command.musicplayer;

import command.Command;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import music.MusicPlayer;

/**
 * This command implements the skip functionality for the music player
 */
public class Skip extends Command {
    public static final String name = "skip";
    public static final String description = "Go to the next track in the playback queue.";

    public Skip(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        MusicPlayer.musicPlayer.next();
    }
}
