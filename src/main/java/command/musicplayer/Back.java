package command.musicplayer;

import command.Command;
import music.MusicPlayer;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

/**
 * Created by evan on 5/2/2018.
 */
public class Back extends Command {
    public static final String name = "back";
    public static final String description = "Go back to the previous track, if one is available.";

    public Back(MessageReceivedEvent event) {
        super(event);
    }

    // back is an extremely heavy operation for Lavaplayer to handle. Forcing the bot to wait for a second
    // while the lavaplayer thread catches up helps improve stability.
    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        MusicPlayer.musicPlayer.previous();
        try {       // let the track scheduler catch up
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}