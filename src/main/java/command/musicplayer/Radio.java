package command.musicplayer;

import api.LastFM;
import command.Command;
import music.MusicPlayer;
import music.Song;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static main.Globals.CMD_CHAR;
import static main.Globals.ENABLE_SCROBBLING;
import static main.Globals.logger;

/**
 * Created by evan on 5/1/2018.
 */
public class Radio extends Command {
    public static final String name = "radio";
    public static final String description = "Queue recommended songs from last.fm data.\n" +
            CMD_CHAR + "radio\n" +
            CMD_CHAR + "radio (maximum tracks to add)" +
            CMD_CHAR + "radio (last.fm user)" +
            CMD_CHAR + "radio (last.fm user) (maximum track)";

    public Radio(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(!MusicPlayer.isRunning()) {
            event.getTextChannel().sendMessage("You cannot use this command while the music player is not running!").queue();
            return;
        }
        if((!ENABLE_SCROBBLING) && (params.isEmpty() || params.matches("^[0-9]*$"))) {
            event.getTextChannel().sendMessage("Scrobbling must be enabled to use personal radio!" +
                    "\nyou can still specify a last.fm user to play radio from.").queue();
            return;
        }

        try {
            int toQueue = 20;
            ArrayList<Song> recommendedSongs;

            if(params.length() > 0) {
                //toQueue = Integer.valueOf(params);
                if(params.matches("^[0-9]*$")) {
                    toQueue = Integer.valueOf(params);

                    Message toSend = new MessageBuilder().append("Generating song list...").build();
                    event.getTextChannel().sendMessage(toSend).queue();

                    recommendedSongs = LastFM.getRecommendedSongs(toQueue / 9);
                } else if(!params.contains(" ")) {
                    Message toSend = new MessageBuilder().append("Generating song list...").build();
                    event.getTextChannel().sendMessage(toSend).queue();

                    recommendedSongs = LastFM.getRecommendedSongs(toQueue / 9, params);
                } else {
                    toQueue = Integer.valueOf(params.split(" ")[1]);
                    String uname = params.split(" ")[0];

                    Message toSend = new MessageBuilder().append("Generating song list...").build();
                    event.getTextChannel().sendMessage(toSend).queue();

                    recommendedSongs = LastFM.getRecommendedSongs(toQueue / 9, uname);
                }

            } else {
                Message toSend = new MessageBuilder().append("Generating song list...").build();
                event.getTextChannel().sendMessage(toSend).queue();

                recommendedSongs = LastFM.getRecommendedSongs(toQueue / 9);
            }

            toQueue = (toQueue > recommendedSongs.size()) ? recommendedSongs.size() : toQueue;

            // Lavaplayer struggles to keep up unless we add in a forced lag time
            event.getTextChannel().sendMessage("Queueing " + toQueue + " songs.").queue();
            for(int i = 0; i < toQueue; i++) {
                MusicPlayer.musicPlayer.queue(recommendedSongs.get(i));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {}
            }

        } catch (IOException e) {
            event.getTextChannel().sendMessage("Error occurred in radio track generation.").queue();
        }
    }
}
