package command;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.reflections.Reflections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static main.Globals.logger;
import static main.Globals.permJSONUrl;

/**
 * Created by evan on 4/30/2018.
 *
 */
public class CommandManifest {
    private static HashMap<String, Class<? extends Command>> commandMap;

    public static void init() {
        commandMap = new HashMap<>();
        Reflections reflections = new Reflections("command");
        Set<Class<? extends Command>> classes = reflections.getSubTypesOf(Command.class);
        for(Class<? extends Command> clazz : classes) {
            logger.config("Command found: " + clazz);
            try {
                commandMap.put(String.valueOf(clazz.getDeclaredField("name").get(new String())), clazz);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        logger.info("registered commands: " + commandMap.keySet());
    }

    /**
     * checks perm.json to see if a user has proper roles to run the command. If they have one of the necessary
     * role, returns an instance of the command with the user's given parameters. If they do not have any of the
     * necessary roles, it will tell the user that they aren't allowed to use that command, then return an
     * instance of the Command base class. If the user gives a command name that doesn't exist, returns an instance of
     * the Command base class. (side note: recall that the Command base class' run(event) method simply sends a message
     * that says "Invalid Command!").
     * @param event The event we are searching for an appropriate Command for.
     * @return The Command whose name parameter matches the event.
     */
    public static Command runByMessageReceivedEvent(MessageReceivedEvent event) {
        String cmd = Command.getCommandName(event.getMessage().getContentDisplay().trim());
        if(commandMap.containsKey(cmd)) try (FileReader permFile = new FileReader(new File(permJSONUrl))) {

            JSONObject perm = new JSONObject(new JSONTokener(permFile));
            JSONArray commandArray = perm.getJSONArray("commands");
            permFile.close();       //just to be safe

            int retrievedIndex = -1;
            for(int i = 0; i < commandArray.length(); i++)
                if(commandArray.getJSONObject(i).getString("name").equals(cmd))
                    retrievedIndex = i;

            if((retrievedIndex == -1) || event.getMember().hasPermission(Permission.ADMINISTRATOR))
                return runCmd(cmd, event);
            else if(senderHasRole(commandArray.getJSONObject(retrievedIndex).getJSONArray("roles"), event))
                return runCmd(cmd, event);
            else {
                event.getTextChannel().sendMessage("You do not have permission to run this command!").queue();
            }

        } catch (FileNotFoundException e) {
            logger.critical("perm.json was not found at dir\n   " + permJSONUrl);
            return runCmd(cmd, event);
        } catch (JSONException e) {
            logger.critical("perm.json has a formatting error that prevented a JSONObject from being generated.");
        } catch (IOException e) {
            e.printStackTrace();
        } return new Command(event);
    }

    public static String generateHelpMessage() {
        int colWidth = 16;
        StringBuilder sb = new StringBuilder();
        sb.append("```").append(rightPadding("Command", colWidth)).append("Description\n");

        // make an array of keys and sort them, so the commands show up in alphabetical order
        Object[] keys = commandMap.keySet().toArray();
        Arrays.sort(keys);
        for(Object n : keys) {
            String name = n.toString();
            sb.append(rightPadding(name, colWidth));
            try {
                sb.append(((String)commandMap.get(name)
                        .getDeclaredField("description")
                        .get(new String())).replace("\n",rightPadding("\n",colWidth + 1)));
            } catch (IllegalAccessException e) {
                logger.critical("Command " + name + " has no description field!!!");
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            sb.append("\n\n");
        }
        return sb.append("```").toString().trim();
    }

    private static Command runCmd(String cmd, MessageReceivedEvent event) {
        try {
            return (Command) getCommandConstructorForMessageReceivedEvent(commandMap.get(cmd)).newInstance(event);
        } catch (IllegalAccessException f) {
            f.printStackTrace();
        } catch (InstantiationException f) {
            f.printStackTrace();
        } catch (InvocationTargetException f) {
            f.printStackTrace();
        } return new Command(event);
    }

    private static boolean senderHasRole(JSONArray rolesArray, MessageReceivedEvent event) {
        for(Role userRole : event.getMember().getRoles())
            for(Object arrayRole : rolesArray)
                if(((String)arrayRole).equals(userRole.getName())) return true;
        return false;
    }

    private static Constructor<?> getCommandConstructorForMessageReceivedEvent(Class<? extends Command> clazz) {
        try {
            return clazz.getConstructor(MessageReceivedEvent.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } return null;
    }

    private interface Int { int func(int k); }
    private static String rightPadding(String str, int width) {
        Int padding = (k) -> (k < 0) ? 0 : (k);
        str += (new String(new char[padding.func(width - str.length())]).replace('\0', ' '));
        return str;
    }
}
