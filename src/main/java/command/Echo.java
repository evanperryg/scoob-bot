package command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

/**
 * Created by evan on 5/8/2018.
 * Sample echo method.
 */
public class Echo extends Command {
    public static final String name = "echo";
    public static final String description = "Echoes user input";

    public Echo(MessageReceivedEvent event) {
        super(event);
    }

    @Override
    public void run() {
        if(params.isEmpty()) return;
        event.getTextChannel().sendMessage(params).queue();
    }
}
