package util;

/**
 * Created by evan on 5/1/2018.
 */
import java.util.Iterator;

public class Deque<Base> implements Iterable<Base> {
    private class Node {
        private Base object;
        private Node left;
        private Node right;

        Node(Base object, Node left, Node right) {
            this.object = object;
            this.left = left;
            this.right = right;
        }

        Node getLeft() {
            return left;
        }

        Node getRight() {
            return right;
        }

        void setLeft(Node left) {
            this.left = left;
        }

        void setRight(Node right) {
            this.right = right;
        }
    }

    private class NodeIterator implements Iterator<Base> {
        Deque<Base> deque;

        NodeIterator(Deque deque) {
            this.deque = deque;
        }

        @Override
        public boolean hasNext() {
            return !deque.isEmpty();
        }

        @Override
        public Base next() {
            return deque.dequeueFront();
        }
    }

    // head.right = front
    // head.left  = rear
    private Node head = new Node(null, null, null);
    private int size;

    public Deque() {
        head.setLeft(head);
        head.setRight(head);
        size = 0;
    }

    public void enqueueFront(Base object) {
        Node newnode = new Node(object, head, head.right);
        head.right.setLeft(newnode);
        head.setRight(newnode);
        ++size;
    }

    public void enqueueRear(Base object) {
        Node newnode = new Node(object, head.left, head);
        head.left.setRight(newnode);
        head.setLeft(newnode);
        ++size;
    }

    public Base dequeueFront() {
        if(isEmpty()) throw new IllegalStateException();
        --size;
        Base out = head.right.object;
        head.right.right.setLeft(head);
        head.setRight(head.right.right);
        return out;
    }

    public Base dequeueRear() {
        if(isEmpty()) throw new IllegalStateException();
        --size;
        Base out = head.left.object;
        head.left.left.setRight(head);
        head.setLeft(head.left.left);
        return out;
    }

    public Base peekFront() {
        return head.right.object;
    }

    public boolean isEmpty() {
        return (head.left == head) && (head.right == head);
    }

    public int getSize() {
        return size;
    }

    /**
    * The Deque iterator pulls from the front of the Deque. Nodes are deleted
    * as the iterator goes through the org.evanperrygrove.Deque. Therefore, if the iterator is used,
    * FIFO is implemented using enqueueRear, and LIFO is implemented using enqueueFront.
    */
    @Override
    public Iterator<Base> iterator() {
        return new NodeIterator(this);
    }

    /**
     * Prints each object in the Deque on its own line, from back to front
     * @return String showing each object in the Deque.
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        Node n = head;
        while(n.left != head) {
            sb.append(n.left.object).append("\n");
            n = n.left;
        }
        return sb.toString();
    }

    /**
     * Prints each object in the Deque on its own line, from front to back
     * @return String showing each object in the Deque.
     */
    public String toStringReverse() {
        StringBuffer sb = new StringBuffer();
        Node n = head;
        while(n.right != head) {
            sb.append(n.right.object).append("\n");
            n = n.right;
        }
        return sb.toString();
    }

}
