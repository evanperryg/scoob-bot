package util

import java.io.PrintStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by evan on 5/9/2018.
 */
class Kotlog(val printStreams: Array<PrintStream> = arrayOf(System.out))
{
    fun info(text: String) {
        print("INFO", text)
    }

    fun critical(text: String) {
        print("CRITICAL", text)
    }

    fun error(text: String) {
        print("ERROR", text)
    }

    fun config(text: String) {
        print("CONFIG", text)
    }

    private fun print(status: String, text: String) {
        for(ps in printStreams) {
            ps.print(status.padEnd(8, ' '))
            val dateFormat: SimpleDateFormat = SimpleDateFormat("yy/MM/dd HH:mm:ss z")
            val date: Date = Date()
            ps.print(dateFormat.format(date) + ": ")
            ps.println(text.replace("\n", "\n                                  "))
        }
    }
}