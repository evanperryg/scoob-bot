package api;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;

import com.google.api.services.youtube.model.*;
import com.google.api.services.youtube.YouTube;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import static main.Globals.logger;

/**
 * This is a lightly-tweaked version of sample code provided by the YouTube Data API documentation.
 */
public class Youtube {
    // increasing resultcount actually degrades the quality of results because we're filtering through more crap.
    // keep it between 5 and 10 for best results
    public static JSONArray getSearchResults(String query) {
        return getSearchResults(query, 7);
    }

    /** Application name. */
    private static final String APPLICATION_NAME = "ScoobBot";

    /** Directory to store user credentials for this application. */
    private static final java.io.File DATA_STORE_DIR = new java.io.File("./properties/private");

    /** Global instance of the {@link FileDataStoreFactory}. */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this quickstart.
     *
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/drive-java-quickstart
     */
    private static final Collection<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/youtube.force-ssl", "https://www.googleapis.com/auth/youtubepartner");

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws IOException Thrown if the connection failed.
     */
    private static Credential authorize() throws IOException {
        // Load client secrets.
        //InputStream in = YouTube.class.getResourceAsStream("/client_secret.json");
        InputStream in = new FileInputStream(DATA_STORE_DIR.getAbsolutePath() + "\\client_secret.json");
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader( in ));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(DATA_STORE_FACTORY)
                .setAccessType("offline")
                .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("user");
        logger.config("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        in.close();
        return credential;
    }

    /**
     * Build and return an authorized API client service, such as a YouTube
     * Data API client service.
     * @return an authorized API client service
     * @throws IOException Thrown if the connection failed.
     */
    public static YouTube getYouTubeService() throws IOException {
        Credential credential = authorize();
        return new YouTube.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public static JSONArray getSearchResults(String query, int resultcount) {
        if((resultcount < 0) || (resultcount > 50))
            throw new IllegalArgumentException("given resultcount is out of bounds (0-50)");
        else try {
            YouTube youtube = getYouTubeService();

            try {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("part", "snippet");
                parameters.put("maxResults", Integer.toString(resultcount));
                parameters.put("q", query);
                parameters.put("type", "video");

                YouTube.Search.List searchListByKeywordRequest = youtube.search().list(parameters.get("part"));
                if (parameters.containsKey("maxResults")) {
                    searchListByKeywordRequest.setMaxResults(Long.parseLong(parameters.get("maxResults")));
                }

                if (parameters.containsKey("q") && parameters.get("q") != "") {
                    searchListByKeywordRequest.setQ(parameters.get("q"));
                }

                if (parameters.containsKey("type") && parameters.get("type") != "") {
                    searchListByKeywordRequest.setType(parameters.get("type"));
                }

                SearchListResponse response = searchListByKeywordRequest.execute();
                logger.info("Youtube API returned results for query: " + query);
                return new JSONObject(response).getJSONArray("items");
            } catch (GoogleJsonResponseException e) {
                e.printStackTrace();
                System.err.println("There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
            } catch (Throwable t) {
                t.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } return null;
    }

    /**
     * Performs the YouTube Data API's getVideo method, and returns the first JSON object in the "items"
     * array within the response.
     * @param address the full URL of a YouTube video link, e.g. youtube.com/watch?v=aaaaaaaa.
     * @return The JSON object for the video.
     */
    public static JSONObject getVideo(String address) {
        address = address.substring((address.indexOf("watch?v=") + 8));
        logger.info("attempting Youtube.getVideo for video id: " + address);
        try {
            YouTube youTube = getYouTubeService();
            HashMap<String, String> params = new HashMap<>();
            params.put("part", "snippet");
            params.put("id", address);

            YouTube.Videos.List videosListByIdRequest = youTube.videos().list(params.get("part"));
            if (params.containsKey("id") && !params.get("id").equals("")) {
                videosListByIdRequest.setId(params.get("id"));
            }

            VideoListResponse response = videosListByIdRequest.execute();

            logger.info("Youtube.getVideo returned results for id: " + address);
            return new JSONObject(response).getJSONArray("items").getJSONObject(0);
        } catch (IOException e) {
            e.printStackTrace();
        } return null;
    }

}
