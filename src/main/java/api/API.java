package api;

import okhttp3.*;

import java.io.*;
import java.util.Date;
import java.util.HashMap;

import static main.Globals.logger;

/**
 * Created by evan on 4/30/2018.
 */
public class API {
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private String targetUrl;
    private HashMap<String, String> parameters;

    long delay = 80;
    long timeOfLastRequest = 0;

    public API(String targetUrl) {
        this.targetUrl = targetUrl;
        parameters     = new HashMap<>();
    }

    /**
     * Put a parameter in the API. An API request will only use the parameter if you tell it to.
     * @param key the name of the parameter.
     * @param value the value of this parameter.
     * @return true if a parameter was overwritten, false if no parameter already existed with that key.
     */
    public boolean putParameter(String key, String value) {
        return parameters.put(key, value) != null;
    }

    /**
     * Checks to see if the given key exists in the API.
     * @param key the key to check for.
     * @return true if a parameter has this key.
     */
    public boolean hasParameter(String key) {
        return parameters.containsKey(key);
    }

    public void setDelayBetweenRequestsMilliseconds(long delay) {
        this.delay = delay;
    }

    public String executeGet(String... params) throws IOException {
        while(new Date().getTime() < (timeOfLastRequest + delay));
        logger.info("executing GET for " + targetUrl);
        StringBuilder sb = new StringBuilder();
        for(String param : params) {
            sb.append(param).append("=").append(parameters.get(param)).append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        timeOfLastRequest = new Date().getTime();
        return get(targetUrl + "?" + sb.toString());
    }

    public String verboseExecuteGet(String... params) throws IOException {
        while(new Date().getTime() < (timeOfLastRequest + delay));
        logger.info("executing GET for " + targetUrl);
        StringBuilder sb = new StringBuilder();
        for(String param : params) {
            sb.append(param).append("=").append(parameters.get(param)).append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        String out = get(targetUrl + "?" + sb.toString());
        logger.info("response:\n" + out);
        timeOfLastRequest = new Date().getTime();
        return out;
    }

    public String executePost(String... params) throws IOException {
        while(new Date().getTime() < (timeOfLastRequest + delay));
        logger.info("executing POST for " + targetUrl);
        StringBuilder sb = new StringBuilder();
        for(String param : params) {
            sb.append(param).append("=").append(parameters.get(param)).append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        timeOfLastRequest = new Date().getTime();
        return post(targetUrl + "?", sb.toString());
    }

    public String verboseExecutePost(String... params) throws IOException {
        while(new Date().getTime() < (timeOfLastRequest + delay));
        logger.info("executing POST for " + targetUrl);
        StringBuilder sb = new StringBuilder();
        for(String param : params) {
            sb.append(param).append("=").append(parameters.get(param)).append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        String out = post(targetUrl + "?", sb.toString());
        logger.info("response:\n" + out);
        timeOfLastRequest = new Date().getTime();
        return out;
    }

    /**
     * Performs a GET request to the specified URL.
     * @param targetURL the URL which will receive the GET.
     * @return A string containing the complete response from the target.
     * @throws IOException Thrown if the request was cancelled, or connection was interrupted or timed out.
     */
    static String get(String targetURL) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(targetURL)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    /**
     * Performs a POST request to the specified URL, using specified parameters.
     * @param targetURL The URL which will receive the POST.
     * @param parameters A string containing all parameters (with their values, of course) for the request.
     * @return A string containing the complete response from the target.
     * @throws IOException Thrown if the request was cancelled, or connection was interrupted or timed out.
     */
    static String post(String targetURL, String parameters) throws IOException {
        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, parameters);
        Request request = new Request.Builder()
                .url(targetURL)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
