package api;

import de.umass.lastfm.*;
import de.umass.lastfm.scrobble.ScrobbleResult;
import music.Song;
import music.YoutubeSong;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static main.Globals.ENABLE_SCROBBLING;
import static main.Globals.LASTFM_APIKEY;
import static main.Globals.logger;

/**
 * Created by evan on 4/30/2018. Methods in LastFM are essentially wrappers for various Last.FM API requests. Before
 * anything else in this class is used, however, the {@code init(JSONObject conf, JSONObject keys)} function should be
 * called.
 */
public class LastFM {
    private static Session session;
    private static String username;

    private static String SECRET;

    /**
     * Initialise the LastFM class. This should be run once at the start of the program.
     * @param conf the JSONObject from conf.json
     * @param keys the JSONObject from keys.json
     */
    public static void init(JSONObject conf, JSONObject keys) {
        logger.info("initializing the LastFM class");
        // populate what we can directly from the JSONObjects
        ENABLE_SCROBBLING = conf.getBoolean("ENABLE_SCROBBLING");
        LASTFM_APIKEY     = keys.getString("LASTFM_APIKEY");
        SECRET            = keys.getString("LASTFM_SHAREDSECRET");

        // required by lastfm-java
        Caller.getInstance().setUserAgent("scoob");

        if(ENABLE_SCROBBLING) {
            // user has asked for scrobbling to be enabled, so try to log into a last.fm account
            logger.config("starting last.fm login");
            String password = "";
            try {
                // try to get the client username from the keys JSONObject
                username = keys.getString("LASTFM_USERNAME");
            } catch (JSONException j) {
                // keys.getString will throw this exception if it doesn't see the username field
                logger.info("username not found in keys.json");

                // prompt the user for their last.fm client username and password
                Console console = System.console();
                System.out.println("Enter last.fm login information");
                username = console.readLine("Username: ");
                password = new String(console.readPassword("Password: "));
            }

            // this if statement will run in the following scenarios:
            // 1) keys.json has a LASTFM_USERNAME field
            // 2) keys.json doesn't have a LASTFM_USERNAME, and the user entered nothing when prompted for their password
            if(password.isEmpty()) try {
                // try to get the password from the keys JSONObject
                password = keys.getString("LASTFM_PASSWORD");
            } catch (JSONException j) {
                // keys.getString will throw this exception if it doesn't find the password field
                logger.info("password not found in keys.json");

                // prompt the user for their last.fm client password
                Console console = System.console();
                System.out.println("Enter password for last.fm account: " + username);
                password = new String(console.readPassword("Password: "));
            }
            session = Authenticator.getMobileSession(username, password, LASTFM_APIKEY, SECRET);
        } else {
            logger.info("scrobbling is currently disabled.");
            logger.info(" To enable scrobbling, set the ENABLE_SCROBBLING variable in conf.json to true.");
        }
    }

    /**
     * Performs a POST to the last.fm artist.getInfo method.
     * @param artistName The name of the artist to search.
     * @return JSONObject returned by artist.getInfo.
     * @throws IOException Thrown
     */
    public static JSONObject getArtistInfo(String artistName) throws IOException {
        return new JSONObject(API.post(
                "http://ws.audioscrobbler.com/2.0/?",
                "method=artist.getinfo&artist=" + artistName +
                        "&autocorrect=1&api_key=" + LASTFM_APIKEY + "&format=json"));
    }

    public static JSONObject getSongInfo(String artistName, String trackName) throws IOException {
        return new JSONObject(API.post(
                "http://ws.audioscrobbler.com/2.0/?",
                "method=track.getinfo&artist=" + artistName +
                        "&track=" + trackName +
                        "&autocorrect=1&api_key=" + LASTFM_APIKEY + "&format=json"));
    }

    public static JSONArray songSearch(String trackName) throws IOException {
        return new JSONObject(API.post(
                "http://ws.audioscrobbler.com/2.0/?",
                "method=track.search&track=" + trackName +
        "&api_key=" + LASTFM_APIKEY + "&format=json")).getJSONObject("results")
                .getJSONObject("trackmatches").getJSONArray("track");
    }

    public static void scrobble(Song song) {
        logger.info("attempting to scrobble: " + song.getTitle() + " by " + song.getArtist());

        int now = (int) (System.currentTimeMillis() / 1000);
        ScrobbleResult result = Track.scrobble(song.getArtist(), song.getTitle(), now, session);

        if(result.isSuccessful() && !result.isIgnored())
            logger.info("scrobble was successful!");
    }

    public static void setNowListening(Song song) {
        logger.info("LastFM.setNowListening(): attempting to set now listening: " + song.getTitle() + " by " + song.getArtist());
        ScrobbleResult result = Track.updateNowPlaying(song.getArtist(), song.getTitle(), session);

        if(result.isSuccessful() && !result.isIgnored())
            logger.info("successfully set now listening!");
    }

    public static void loveTrack(Song song) {
        logger.info("LastFM.loveTrack(): attempting to love " + song.getTitle() + " by " + song.getArtist());
        Result result = Track.love(song.getArtist(), song.getTitle(), session);

        if(result.isSuccessful())
            logger.info("loved track!");
    }


    public static void unloveTrack(Song song) {
        logger.info("LastFM.loveTrack(): attempting to love " + song.getTitle() + " by " + song.getArtist());
        Result result = Track.love(song.getArtist(), song.getTitle(), session);

        if(result.isSuccessful())
            logger.info("loved track!");
    }

    public static ArrayList<Song> getRecommendedSongs(int k) throws IOException {
        return getRecommendedSongs(k, username);
    }

    /**
     * Gets recommended songs based on the last.fm user's 1-month listening history
     * @param k the "recommendation factor." Higher values = more recommended songs. Should be set between 2 and 10.
     * @return an ArrayList of Songs recommended by the algorithm.
     */
    public static ArrayList<Song> getRecommendedSongs(int k, String username) throws IOException {
        k = (k > 10) ? 10 : ((k < 2) ? 2 : k);
        ArrayList<Song> out = new ArrayList<>();
        JSONObject topTracks = null;
        API api = new API("http://ws.audioscrobbler.com/2.0/");
        api.setDelayBetweenRequestsMilliseconds(150);   //force at least 150ms delay between api requests
        api.putParameter("api_key", LASTFM_APIKEY);
        api.putParameter("format", "json");
        api.putParameter("user", username);
        api.putParameter("period", "1month");
        api.putParameter("limit", "30");        // should be greater than 10
        api.putParameter("autocorrect","1");


        api.putParameter("method", "user.gettoptracks");
        topTracks = new JSONObject(api.executePost("method", "period", "user", "limit", "api_key", "format"));
        topTracks = topTracks.getJSONObject("toptracks");

        if(topTracks != null) {
            JSONArray topTrackList = topTracks.getJSONArray("track");
            // find recommended tracks for each of the top 20 tracks of this month, or however many tracks are available from the request
            int maxFindRecs = (topTrackList.length() > 20) ? 20 : topTrackList.length();

            api.putParameter("method", "track.getsimilar");
            for(int i = 0; i < maxFindRecs; i++) {
                String playedArtist = topTrackList.getJSONObject(i).getJSONObject("artist").getString("name");
                String playedTitle  = topTrackList.getJSONObject(i).getString("name");
                api.putParameter("artist", playedArtist);
                api.putParameter("track", playedTitle);
                logger.info("Attempting to retrieve recommended tracks for " + playedTitle + " by " + playedArtist);
                JSONObject recommendedTracks = new JSONObject(
                        api.executePost("method", "track", "artist", "autocorrect", "limit", "api_key", "format"));
                try {
                    JSONArray recList = recommendedTracks.getJSONObject("similartracks").getJSONArray("track");
                    int toAdd = (k > recList.length()) ? recList.length() : k;
                    logger.info("LastFM.getRecommendedSongs: will try to add " + toAdd + " tracks recommended for " + playedTitle + " by " + playedArtist);
                    while(toAdd-- > 0) {
                        int rand = ThreadLocalRandom.current().nextInt(0, recList.length());
                        JSONObject trackElement = recList.getJSONObject(rand);
                        String artist = trackElement.getJSONObject("artist").getString("name");
                        String title  = trackElement.getString("name");

                        // check that the song isn't already in the list, then add it
                        if(!listHasSong(out, title, artist)) {
                            logger.info("LastFM.getRecommendedSongs: adding " + title + " by " + artist);
                            if(out.size() == 0)
                                out.add(YoutubeSong.generateYoutubeSongWithoutMetadata(title, artist, "Recommended based on " + playedTitle + " by " + playedArtist));
                            else out.add(ThreadLocalRandom.current().nextInt(0, out.size()),
                                    YoutubeSong.generateYoutubeSongWithoutMetadata(title, artist, "Recommended based on " + playedTitle + " by " + playedArtist));
                        }

                        recList.remove(rand);
                    }
                } catch (JSONException e) {
                    logger.error("Could not generate recommendations for " + playedTitle + " by " + playedArtist);
                }
            }

            // randomly add in 30% of the users top monthly tracks, up to 9.
            int tracksToAdd = (int) (topTrackList.length() * 0.3);
            logger.info("LastFM.getRecommendedSongs: will try to add " + tracksToAdd + " tracks from topTracks.");
            while(tracksToAdd-- > 0) {
                int rand = ThreadLocalRandom.current().nextInt(0, topTrackList.length());
                JSONObject trackElement = topTrackList.getJSONObject(rand);
                String artist = trackElement.getJSONObject("artist").getString("name");
                String title  = trackElement.getString("name");

                // check that the song isn't already in the list, then add it
                if(!listHasSong(out, title, artist)) {
                    logger.info("LastFM.getRecommendedSongs: adding " + title + " by " + artist);
                    out.add(ThreadLocalRandom.current().nextInt(0, out.size()),
                            YoutubeSong.generateYoutubeSongWithoutMetadata(title, artist, "Recently played track"));
                }

                topTrackList.remove(rand);
            }
        }
        return out;
    }

    private static boolean listHasSong(List<Song> songList, String title, String artist) {
        for(Song inList : songList)
            if((inList.getArtist().equals(artist)) && (inList.getTitle().equals(title))) return true;
        return false;
    }

    /** @deprecated kept around to show an example of how auth tokens are generated. */
    private static String getToken() throws IOException {
        logger.info("updating last.fm auth token");
        String urlParams = "method=auth.gettoken&api_key=" + LASTFM_APIKEY + "&format=json";
        JSONObject lastfmtoken = new JSONObject(API.post("http://ws.audioscrobbler.com/2.0/?", urlParams));
        return lastfmtoken.getString("token");
    }
}
